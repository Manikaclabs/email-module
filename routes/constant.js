/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}


exports.responseStatus = {};

define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 101);


exports.responseMessage = {}; 

define(exports.responseMessage, "email_sending_failed", "email sending failed");
define(exports.responseMessage, "email_sending", "Email sent");
exports.deviceType = {};
