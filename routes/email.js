var express = require('express');
var router = express.Router();
var func = require('./commonfunction');
var sendResponse = require('./sendResponse');
var constant = require('./constant');

router.post('/send_email',function(req, res)
{
    var message = req.body.message;
    var id = req.body.id;
    var subject = req.body.subject;

    func.sendEmail(message,subject,id,function(callback)
    {
        if(callback==0)
        {
            sendResponse.sendErrorMessage(constant.responseMessage.email_sending_failed,res);
        }
        else
        {
            sendResponse.sendSuccessData(constant.responseMessage.email_sending,res);
        }
    });
    
    
});


module.exports = router;




